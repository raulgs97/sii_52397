#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main (void)
{
	int fd;
	char ch;
	char* nombre = "/tmp/FIFOtenis";

	if (mkfifo(nombre,0666) == 0)
	{
		fd = open (nombre, O_RDONLY);
		if (fd== -1){
			perror ("error open");
			return 1;
		}
		
		while (read (fd,&ch,1) == 1)
			write (1,&ch,1);
	
		close (fd);
		unlink (nombre);

	}
	else
	{
		perror ("Error al crear FIFO");
	}

	return 1;

}
